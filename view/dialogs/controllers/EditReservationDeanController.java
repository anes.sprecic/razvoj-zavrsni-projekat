package view.dialogs.controllers;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

import java.time.temporal.ChronoField;

import api.Api;
import entities.*;

public class EditReservationDeanController {
	private Reservation reservation = null;

	
	@FXML
	public Button btnConfirm;
	@FXML
	public Label lblError;
	@FXML
	public TextField inputStartHour;
	@FXML
	public TextField inputDuration;
	@FXML
	public TextField inputDescription;
	@FXML
	public DatePicker datePicker;
	@FXML
	public ChoiceBox<Classroom> dropdownClassrooms;
	@FXML
	public ChoiceBox<Teacher> dropdownTeachers;

	public Reservation getReservation() {
		return reservation;
	}
	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
		if(reservation != null)
			updateInfo();
	}
	private void updateInfo() {
		inputDescription.setText(reservation.getDescription());
		inputStartHour.setText(Integer.toString(reservation.getStartHour()));
		inputDuration.setText(Integer.toString(reservation.getDuration()));
		datePicker.setValue(reservation.getDate());
		dropdownClassrooms.setItems(FXCollections.observableArrayList(Api.getClassrooms()));
		dropdownClassrooms.setValue(reservation.getClassroom());
		dropdownTeachers.setItems(FXCollections.observableArrayList(Api.getTeachers()));
		dropdownTeachers.setValue(reservation.getTeacher());
	}
	@FXML
	private void initialize() {
		dropdownClassrooms.setItems(FXCollections.observableArrayList(Api.getClassrooms()));
		dropdownTeachers.setItems(FXCollections.observableArrayList(Api.getTeachers()));

	}
	@FXML
	private void handleConfirm(ActionEvent e) {
		Reservation newVal = new Reservation();
		if(	inputDescription.getText().equals("") ||
			inputStartHour.getText().equals("") ||
			inputDescription.getText().equals("") ||
			dropdownClassrooms.getValue() == null ||
		    dropdownTeachers.getValue() == null ||
			datePicker.getValue() == null 
			) {
			lblError.setText("Ispunite sva polja!");
			return;
		}

		try {
			int duration =(new Integer(inputDuration.getText()));
			int startHour = (new Integer(inputStartHour.getText()));
			if(startHour < 8 || startHour > 19) {
				lblError.setText("Moguce je rezervisati samo u terminu 8:00 do 20:00!");
				return;
			}
			if(duration > 11) {
				lblError.setText("Dozvoljeno trajanje je maksimalno 11h!");
				return;
			}
			if(duration < 1) {
				lblError.setText("Minimalno trajanje je 1h!");
				return;
			}
			if(startHour + duration > 20) {
				lblError.setText("Trajanje premasuje 20:00!");
				return;
			}
		}catch(Throwable err) {
			lblError.setText("Dozvoljeni su samo brojevi za polja pocetak i trajanje!");
			return;
		}
		
		if(datePicker.getValue().get(ChronoField.DAY_OF_WEEK) == 7) {
			lblError.setText("Moguce je rezervisati prostor samo danima ponedjeljak - subota!");
			return;
		}

		newVal.setTeacher(dropdownTeachers.getValue());
		newVal.setClassroom(dropdownClassrooms.getValue());
		newVal.setDate(datePicker.getValue());
		newVal.setDuration(new Integer(inputDuration.getText()));
		newVal.setStartHour(new Integer(inputStartHour.getText()));
		newVal.setDecription(inputDescription.getText());

		if(reservation == null) {
			Api.insertReservation(newVal);
		}else {
			Api.updateReservation(reservation, newVal);
		}
		
		Stage appStage=(Stage)btnConfirm.getScene().getWindow();
		Window window = btnConfirm.getScene().getWindow();

		window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
	}
	@FXML
	private void handleCancel(ActionEvent e) {
		Window window = btnConfirm.getScene().getWindow();

		window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
		
	}
}


