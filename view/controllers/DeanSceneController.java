package view.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class DeanSceneController extends ProfessorSceneController {
	
	@FXML
	public Button btnUnos;
	
	@FXML
	private void handleUnos() {
		
		try {
			Stage dialog = new Stage();
			dialog.initModality(Modality.NONE);
			Stage appStage=(Stage)btnUnos.getScene().getWindow();
			dialog.initOwner(appStage);
	
		    FXMLLoader loader = new FXMLLoader(getClass().getResource("../MenuUredjivanje.fxml"));
	        Parent root = loader.load();
//	        IzvjestajController c = loader.getController();
//	        c.generate(loggedInUser.getTeacher(), datePicker.getValue());
	        Scene scene=new Scene(root);
	        dialog.setScene(scene);
	        dialog.setTitle("Uredjivanje");
	        dialog.setOnCloseRequest(event -> {rerender();});     
	        dialog.show();
		}
		catch(Throwable e) {System.out.println(e);}
	}

}
