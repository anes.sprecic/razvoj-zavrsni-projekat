package view.tables.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import api.Api;
import api.Api.DatabaseError;
import entities.Classroom;
import entities.Subject;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import main.Main;

public class SubjectsTableController implements Initializable {
	
	@FXML
	private TableView<Subject> tableView;
	@FXML
	private TableColumn<Subject, String> nameColumn;
	@FXML
	private TextField NameTextField;
	@FXML
	private Label lblError;

	
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
	        nameColumn.setCellValueFactory(new PropertyValueFactory<Subject, String>("name"));
	        tableView.setItems(FXCollections.observableArrayList(Api.getSubjects()));
	        tableView.setEditable(true);
	        nameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
	        
	        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

	}    
	 
	
	public void rerender() {
        tableView.setItems(FXCollections.observableArrayList(Api.getSubjects()));
	}

	
	@FXML
    public void handleNameCellEvent(CellEditEvent edittedCell)
    {
        Subject subjectSelected =  tableView.getSelectionModel().getSelectedItem();
        Subject newVal = new Subject();
        newVal.setName(edittedCell.getNewValue().toString());
        Api.updateSubject(subjectSelected, newVal);
        rerender();
    }
	
	@FXML 
	public void handleBtnAddItem() {
		Subject newSubject = new Subject();
		if(NameTextField.getText().length() != 0) {
			newSubject.setName(NameTextField.getText());
			Api.insertSubject(newSubject);
			rerender();
	        NameTextField.clear();
		}
	}
	
	@FXML
	public void handleBtnDeleteItem() {
	ObservableList<Subject> selectedRows = tableView.getSelectionModel().getSelectedItems();

	       if(selectedRows.size() != 0) {
			   Main.showConfirmDialog("Da li ste sigurni da zelite izbrisati predmet?",
						bool -> {
							if(bool) {
								    for (Subject row : selectedRows) {
								    	try {DatabaseError e = Api.deleteSubject(row);
								    	if(e.equals(DatabaseError.OK))
										     rerender();
									    	else
									    	 lblError.setText("Nemoguce izbrisati predmet, koristi se u drugim entitetima u bazi!");	}
								    	catch(Exception e) {
								    		e.printStackTrace();
								    	}
							       
								    }
							}});
		            }
		
	 }
	
}
