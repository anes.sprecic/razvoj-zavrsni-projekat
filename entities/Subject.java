package entities;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="SUBJECTS")
public class Subject implements Serializable
{

	@Id
	@TableGenerator(name = "SubjectGenerator")
	@GeneratedValue(generator = "SubjectGenerator")
	@Column(name="SUBJECT_ID")
	private int id;
    public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String toString() {
		return name;
	}

}
