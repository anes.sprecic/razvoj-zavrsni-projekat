package main;
import java.io.IOException;

import api.Api;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import view.dialogs.controllers.ConfirmDialogController;



public class Main extends Application {

    private static Stage primaryStage;
    private Pane rootLayout;

    public interface BoolLambda{
		public void func(Boolean e);
	}

	public static void showConfirmDialog(String promptText, BoolLambda lambda) {
		try {
			Stage dialog = new Stage();
			// freeze the rest until this dialog is done
			dialog.initModality(Modality.APPLICATION_MODAL);
			dialog.initOwner(primaryStage);
	
		    FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("../view/dialogs/ConfirmDialog.fxml"));
	        Parent root = loader.load();
	        ConfirmDialogController c =  loader.getController();
			c.setLambda(lambda);
	        c.setPromptText(promptText);
	        Scene scene=new Scene(root);
	        dialog.setScene(scene);
	        dialog.setTitle("Potvrda");
	        dialog.show();
		}
		catch(Throwable e) {System.out.println(e);}
	}
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Raspored");

        initRootLayout();

    }
    
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("../view/LoginScene.fxml"));
            rootLayout = (Pane) loader.load();
            
            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
	        scene.getStylesheets().add("view/stylesheet.css");
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    
    public static void main(String[] args) {
    	Api.insertTestData();
        launch(args);
    }
}